<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<title>Info windows</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="Resources/css/default.css">
	
   
</head>

<body>

<header>
		<span class="toggle-button">
        	<div class="menu-bar menu-bar-top"></div>
        	<div class="menu-bar menu-bar-middle"></div>
        	<div class="menu-bar menu-bar-bottom"></div>
        </span>
		<div class="menu-wrap">
			<div class="menu-sidebar">
				<ul class="menu">
					<li>			
					<div class="file-field input-field">
      <div class="btn-small waves-effect waves-orange deep-orange lighten-3">
        <span>File</span>
        <input type="file" id="fileInput" >
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
			</li>                  
	<li> <button type="button" id="show-listings" class="waves-effect  waves-dark btn-small teal lighten-4" >Show Listings</button></li>
					<li><button type="button" id= "hide-listings" class="waves-effect  waves-dark btn-small blue">Hide Listings</button></li>					
					<li> <button type="button" id="toggle-drawings" class="waves-effect waves-light btn-small red">Drawing Tools</button></li>
					<li>
					 <div id="areaInfo"></div>
					 <div id="marker-count"></div>	
					</li>
					<li><hr></li>
					<li>
					<input type="text" id= "zoom-to-area-text" placeholder="enter your favourite area">
					<button type="button" class="btn-small waves-effect waves-orange green" id="zoom-to-area">Zoom</button>
					</li>
					<li>
					<span>With In:</span>
				 <div class="input-field">
				 <select id="max-duration">
				 <option value='10'>10 minutes</option>
				 <option value='20'>20  minutes</option>
				 <option value='30'>30  minutes</option>
				 <option value='40'>40  minutes</option>
				 <option value='50'>50  minutes</option>			
				 <option value='60'> 1 hour</option>			
				 </select>
				 </div></li>
				 <li>
				   <div class="input-field">
				  <select id="mode">
				 <option value='DRIVING'>DRIVING</option>
				 <option value='WALKING'>WALKING</option>
				 <option value='BICYCLING'>BICYCLING</option>
				 <option value='TRANSIT'>TRANSIT</option>
				 
				 </select>
				  </div>
				 </li>
				 <li> <span class="text">Of</span>
				 <input type="text" id="search-within-time-text" placeholder="example">
			</li>
				 <li>	 <input type="button" class= "waves-effect waves-dark btm-small red accent-3 " id="search-within-time" value="GO"></li>
				</ul>			
			</div>
		</div>
	</header>
	
		<div class="wrapper">
		<section class="text">
		  <div id="map"></div>
		</section>
	</div>
 
<!--<div id="map-canvas" style="height: 500px; width: 500px;"></div>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
<script src="Resources/js/maps.js"></script>
<script 
src=
"https://maps.googleapis.com/maps/api/js?language=es&libraries=drawing,geometry&key=AIzaSyA71rwaTVAgSVVzXbse7nMStRtMFnycn5c">
</script>

			<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script src="Resources/js/directions.js"></script>

<script>

  </script>
</body>
</html>