   
            $(document).ready(function(){
                var markers = []; // define global array in script tag so you can use it in whole page  
         var defaultIcon = makeMarkerIcon('0091ff');
            var highlightedIcon = makeMarkerIcon('e1e1e1');	
  var largeInfowindow = new google.maps.InfoWindow();			
                var myCenter=new google.maps.LatLng(8.983333,-79.516670);
                var mapProp = {
                        center:myCenter, 
                        zoom:6,
                        minZoom:6,          
                        mapTypeId : google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: true  ,
	mapTypeControlOptions:{         
			position: google.maps.ControlPosition.TOP_LEFT,
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU						
                    }
				}
                //google map object       
                 var map = new google.maps.Map(document.getElementById("map"),mapProp); 

                  //change event of input tag where type=file and  id=filename 
                  $("#filename").change(function(e) {

                     var ext = $("input#filename").val().split(".").pop().toLowerCase();

                     if($.inArray(ext, ["csv"]) == -1) {
                            alert('Upload CSV');
                            return false;
                      }

                     if (e.target.files != undefined) {

                            var reader = new FileReader();
                            reader.onload = function(e) {

                                      var csvval=e.target.result.split("\n");
                                      var csvvalue;                                          

                                      for(var i = 0;i < csvval.length;i++)
                                      {
                                              markers[i] = []; 
                                              csvvalue = csvval[i].split(",");
                                              markers[i][0] = csvvalue[0]; //id
											  var title = csvval[1];
											  var City = csvval[2];										
                                              var lat = parseFloat(csvvalue[3]); //latitude
                                              var lng = parseFloat(csvvalue[4]); //longitude
											  for(var j=0; j < markers.length; j++ )
											  {
                                              markers[j] = new google.maps.Marker({
                                                   position: new google.maps.LatLng(lat,lng),
												    animation: google.maps.Animation.DROP,
                                                   map: map,
												   icon: defaultIcon,
													title: title,
													city: City												   
                                              });
											  
											  							   
					google.maps.event.addListener(markers[j],'click', function () {
                populateInfowindow(this,largeInfowindow);
					});
            }

                                       }
		
   google.maps.event.addListener(markers,'mouseover', function () {
                this.setIcon(highlightedIcon);
            });
               google.maps.event.addListener(markers,'mouseout', function () {
                this.setIcon(defaultIcon);
            });	
			
			     var drawingManager = new google.maps.drawing.DrawingManager(
            {
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [
                        google.maps.drawing.OverlayType.POLYGON
                    ]
                }
            });
			
			     drawingManager.addListener('overlaycomplete', function (event) {
if (polygon) {
                polygon.setMap(null);
                hidelistings();
            }

            drawingManager.setDrawingMode(null);
            polygon = event.overlay;
            polygon.setEditable(true);
            searchWithinPolygon();
            polygon.getPath().addListener('set_at', searchWithinPolygon);
            polygon.getPath().addListener('insert_at', searchWithinPolygon);
            var area = google.maps.geometry.spherical.computeArea(polygon.getPath());
            // var area = google.maps.geometry.spherical.computeArea(polygon.getPath());
            //window.alert(area + "SQUARE METERS");
            document.getElementById('areaInfo').innerHTML = area + "SQUARE METERS";

        });
		
			
		function geolocate()
{
    if(navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(function(position)
        {
         var geolocation =
             {
                 lat: position.coords.latitude,
                 lng: position.coords.longitude
             };
         var circle= new google.maps.Circle({
             center:geolocation,
             radius:position.coords.accuracy
         });
            // new google.maps.places.Autocomplete.setBounds(circle,map.getBounds());
			  google.maps.event.addListener(map, 'bounds_changed', function() {
         //alert(map.getBounds());
		 new google.maps.places.Autocomplete.fitBounds(circle,getBounds());
      });
        });

    }
}
			
			 document.getElementById('show-listings').addEventListener('click', showlistings);
        document.getElementById('hide-listings').addEventListener('click', hidelistings);
        document.getElementById('toggle-drawings').addEventListener('click', function () {
            toggleDrawing(drawingManager);
        });
     
        document.getElementById('search-within-time').addEventListener('click', function () {
		
            searchWithinTime();
			
        });

                             };
                             reader.readAsText(e.target.files.item(0));
                       }

                     return false;

                    });
					
			
					
					  function populateInfowindow(markers, infowindow) {
            if (infowindow.markers != markers) {
                infowindow.setContent('');
                infowindow.markers = markers;
                //infowindow.setContent('<div>' + marker.title + '</div>');
                infowindow.open(map, markers);
                infowindow.addListener('closeclick', function () {
                    // infowindow.setMarker(null);
                    infowindow.markers = null;
                });
			}
                var streetView = new google.maps.StreetViewService();
                var radius = 50;
//var location = new google.maps.LatLng(latitude,longitude);
                function getStreetView(data,status) {
                    if (status == google.maps.StreetViewStatus.OK) {
                        var nearStreetViewLocation = data.location.latLng;
                        var heading = google.maps.geometry.spherical.computeHeading(nearStreetViewLocation, markers.position);
                        infowindow.setContent('<div>' + markers.title + '</div><div id="pano"></div>');
                        var panoramaOptions = {
                            position: nearStreetViewLocation,
                            pov: {
                                heading: heading,
                                pitch: 30

                            }

                        };
                        var panorama = new google.maps.StreetViewPanorama(
                            document.getElementById('pano'), panoramaOptions);


                    } else {
                        infowindow.setContent('<div>'+ markers.title + '<br>' + markers.route + 'no street view found</div>');
                    }
                }

                streetView.getPanoramaByLocation(markers.position, radius, getStreetView);
                infowindow.open(map, markers);

            

        }
		
		
        function makeMarkerIcon(markerColor) {
            var markerImage = new google.maps.MarkerImage(
                'http://chart.googleapis.com/chart?chst=d_map_spin&chld=1.15|0|' + markerColor + '|40|_|%E2%80%A2',
                new google.maps.Size(21, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(10, 34),
                new google.maps.Size(21, 34));
            return markerImage;
        }
            });
 
        