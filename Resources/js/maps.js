window.onload =function(){
    var fileInput = document.getElementById('fileInput');
  //  var fileDisplayArea = document.getElementById('fileDisplayArea');

    fileInput.addEventListener('change', function (e) {
        var file = fileInput.files[0];
        var textType = /csv.*/;
        var companies;

        // Check if csv file. If so, then run program. If not, show error.
        // if (file.type.match(textType)) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var text = reader.result;

            // Log for debug. Success.
             //console.log(text);

            // Parse csv file and make objects to store in companies array.
            function csvParse(csv) {
                var lines = csv.split("\n");

                // Log for debug. Success.
                // console.log(lines);

                var result = [];
                var headers = lines[0].split(",");

                for (var i = 1; i < lines.length; i++) {
                    var obj = {};
                    var currentline = lines[i].split(",");

                    for (var j = 0; j < headers.length; j++) {
                        obj[headers[j]] = currentline[j];
                    }

                    result.push(obj);
                }

                return result;
            }

            // Store objects in companies.
            companies = csvParse(text);


    var markers = [];

    var polygon = null;
    var map;
	
           

    function initMap() {
	var directionsService = new google.maps.DirectionsService;

        var styles =
            [
                {
                    featureType: 'water',
                    stylers:
                        [
                            {color: '19a0d8'}
                        ]
                },
                {
                    featureType: 'administrative',
                    elementype: 'labels.text.stroke',
                    stylers:
                        [
                            {
                                color: 'ffffff'
                            },
                            {weight: 6}
                        ]
                },
                {
                    featureType: 'administrative',
                    elementType: 'labels.text.fill',
                    stylers: [
                        {color: '000000'}
                    ]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [
                        {
                            color: 'efe9e4'
                        },
                        {lightness: -50}
                    ]
                },
                {
                    featureType: 'transit.station',
                    stylers:
                        [
                            {weight: 9},
                            {hue: 'e85113'}
                        ]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.icon',
                    stylers:
                        [
                            {visibility: 'off'}
                        ]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.fill',
                    stylers: [
                        {
                            color: 'dddddd'
                        },
                        {lightness: -25}
                    ]
                }
            ];
			var LatLng =new google.maps.LatLng(9.113193333
,-79.33470333
);
			
       var  map = new google.maps.Map(document.getElementById('map'), {
            center: LatLng, 
            zoom: 10,
			//map: map,
           // styles: styles,
            mapTypeControl: true,
			mapTypeControlOptions:{
            mapTypeIds:[google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.ROADMAP,google.maps.MapTypeId.TERRAIN,google.maps.MapTypeId.HYBRID],
			position: google.maps.ControlPosition.TOP_RIGHT,
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
			zoomControl: false,
			 streetViewControl: false,
			   fullscreenControl: false
             
			}
        });
		 var transitLayer = new google.maps.TransitLayer(document.getElementById('map'));
          transitLayer.setMap(map);


      

        var drawingManager = new google.maps.drawing.DrawingManager(
            {
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [
                        google.maps.drawing.OverlayType.POLYGON
                    ]
                }
            });

        for (var company in companies) {
			
	  var largeInfowindow = new google.maps.InfoWindow();
            var defaultIcon = makeMarkerIcon('0091ff');
            var highlightedIcon = makeMarkerIcon('e1e1e1');
         	 //var latitude = parseFloat(companies[company].lat);
           // var longitude = parseFloat(companies[company].lng);
           var  markerPosition = new google.maps.LatLng( parseFloat(companies[company].lat), parseFloat(companies[company].lng));
            var id = companies[company].ID;
            var Trade = companies[company].name;
            var city = companies[company].City;
           var  title = companies[company].name;
			var route = companies[company].Route;
            var marker = new google.maps.Marker({            
                position: markerPosition,
                title: title,
                id: id,
                name: Trade,
                city: city,
                icon: defaultIcon,
				route: route,
                animation: google.maps.Animation.DROP              
            });
            markers.push(marker);
            // bounds.extend(marker.position);
		
            marker.addListener('click', function () {
                populateInfowindow(this, largeInfowindow);
            });

            marker.addListener('mouseover', function () {
                this.setIcon(highlightedIcon);
            });
            marker.addListener('mouseout', function () {
                this.setIcon(defaultIcon);
            });
			
				//var bounds= new google.maps.LatLngBounds();
	


        
		//console.log(marker);

        // map.fitBounds(bounds);

    
		//calculateAndDisplayRoute(directionsDisplay,directionsService,stepDisplay,markers,map);
		}

    document.getElementById('show-listings').addEventListener('click', showlistings);
        document.getElementById('hide-listings').addEventListener('click', hidelistings);
        document.getElementById('toggle-drawings').addEventListener('click', function () {
            toggleDrawing(drawingManager);
        });		
		
		   document.getElementById('search-within-time').addEventListener('click', function () {
		
            searchWithinTime();
			
        });
	
		/*document.getElementById('search-within-time-text').addEventListener('onFocus', function(){
			geolocate();
				//calculateAndDisplayRoute(directionsDisplay,directionsService,stepDisplay,map,markerPosition);
		});*/
		
		  
        drawingManager.addListener('overlaycomplete', function (event) {
if (polygon) {
                polygon.setMap(null);
                hidelistings();
            }

            drawingManager.setDrawingMode(null);
            polygon = event.overlay;
            polygon.setEditable(true);
            searchWithinPolygon();
            polygon.getPath().addListener('set_at', searchWithinPolygon);
            polygon.getPath().addListener('insert_at', searchWithinPolygon);
            var area = google.maps.geometry.spherical.computeArea(polygon.getPath());
            // var area = google.maps.geometry.spherical.computeArea(polygon.getPath());
            //window.alert(area + "SQUARE METERS");
            document.getElementById('areaInfo').innerHTML = area + "SQUARE METERS";

        });
		
		/*function geolocate()
{
    if(navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(function(position)
        {
         var geolocation =
             {
                 lat: position.coords.latitude,
                 lng: position.coords.longitude
             };
         var circle= new google.maps.Circle({
             center:geolocation,
             radius:position.coords.accuracy
         });
            // new google.maps.places.Autocomplete.setBounds(circle,map.getBounds());
			  google.maps.event.addListener(map, 'bounds_changed', function() {
         //alert(map.getBounds());
		 new google.maps.places.Autocomplete.fitBounds(circle,getBounds());
      });
        });

    }
}*/
//var latlng = new google.maps.LatLng(latitude,longitude);
	/*	var address= new google.maps.places.Autocomplete((document.getElementById('search-within-time-text')),
    {
        type:['store']
    });
	address.setComponentRestrictions({
    'country': ['PA']
});*/

    		function displayDirections(origin)
{
	hidelistings();
	
	 var destinationAddress = document.getElementById('search-within-time-text').value;
	 var mode = document.getElementById('mode').value;
	 
	 directionsService.route({
		 origin:origin,
		 destination:destinationAddress,
		 travelMode: new google.maps.TravelMode[mode]
	 },function(response,status)	 
	 {
		 if(status == google.maps.DirectionsStatus.OK){
			 var setDirections = new google.maps.DirectionsRenderer(
			 {
				 map:map,
				 directions:response,
				 draggable: true,
				 polylineOptions:{
					 strokeColor: 'green'
				 }
			 });
		 }else{
			 window.alert("Directions service Failed to show route Due to" + status);
		 }
	 }
	 );
}
	
		function searchWithinTime()
		{		
			var distanceMatrix = new google.maps.DistanceMatrixService();			
 var geocoder = new google.maps.Geocoder(markers);
 var getAddress = document.getElementById('search-within-time-text').value;
			if(getAddress=='')
			{
				window.alert('you must choose an address for destination');
				
			}else{
				hidelistings();
				var origins = [];
					
				for(var i=0; i < markers.length; i++)
				{
					origins[i]=markers[i].position;
				}
			var destination= getAddress;
				var mode = document.getElementById('mode').value;
				distanceMatrix.getDistanceMatrix({
					origins: origins,
					destinations: [destination],
					travelMode:google.maps.TravelMode[mode],
					unitSystem:google.maps.UnitSystem.IMPERIAL,		
				},function(response,status){
					if(status!== google.maps.DistanceMatrixStatus.OK)
					{
						window.alert('Error was:' + status);
					}else{
						displayMarkersWithinTime(response);
						//calculateAndDisplayRoute(response,directionsDisplay,directionsService,origin,destination,map);
					}
				}
				);
				
			}
			
		}
		

		
		function displayMarkersWithinTime(response)
		{
			var maxDuration = document.getElementById('max-duration').value;
			var origins = response.originAddresses;
			var destinations = response.destinationAddresses;
			var atLeastOne = false;
			
			for(var i=0; i< origins.length; i++)
			{
				var results = response.rows[i].elements;
				
				for(var j=0; j <results.length; j++)
				{
					var element = results[j];
					if(element.status === "OK")
					{
						var distanceText = element.distance.text;
						var duration = element.duration.value/60;
						var durationText = element.duration.text;
						if(duration <= maxDuration)
						{
							markers[i].setMap(map);
							atLeastOne=true;
							var infowindow = new google.maps.InfoWindow(
							{
								content: '<div class="paddings">' + " " + durationText + " "  + 'away, ' + "<br>" + distanceText +'<div><input type=\"button\" value = \"View Route\" onclick=' + '\"directionsDisplay(&quot;' + origins[i] + '&quot;);\">' + '</div></div>'
										
		});
								
							infowindow.open(map, markers[i]);
							
							markers[i].infowindow = infowindow;
							google.maps.event.addListener( markers[i], 'click', function(){
								this.infowindow.close();
							});
						}
						
						
					}
				}
				
			}
			if(!atLeastOne)
				{
					window.alert('We could not find any locations within that distance!');
				}
			
		}

        function populateInfowindow(marker, infowindow) {
            if (infowindow.marker != marker) {
                infowindow.setContent('');
                infowindow.marker = marker;
                //infowindow.setContent('<div>' + marker.title + '</div>');
                infowindow.open(map, marker);
                infowindow.addListener('closeclick', function () {
                    // infowindow.setMarker(null);
                    infowindow.marker = null;
                });
			}
                var streetView = new google.maps.StreetViewService();
                var radius = 50;
var location = new google.maps.LatLng(markers);
                function getStreetView(data,status) {
                    if (status == google.maps.StreetViewStatus.OK) {
                        var nearStreetViewLocation = data.location.latLng;
                        var heading = google.maps.geometry.spherical.computeHeading(nearStreetViewLocation, marker.position);
                        infowindow.setContent('<div>' + marker.title + '</div><div id="pano"></div>');
                        var panoramaOptions = {
                            position: nearStreetViewLocation,
                            pov: {
                                heading: heading,
                                pitch: 30

                            }

                        };
                        var panorama = new google.maps.StreetViewPanorama(
                            document.getElementById('pano'), panoramaOptions);


                    } else {
                        infowindow.setContent('<div> Name:'+ marker.title + '<br>' + 'Route:' + marker.route + '<br>'+ 'City:'+marker.city + '<br>'+ 'Marchent:' + marker.name + '<br>no street view found</div>');
                    }
                }

                streetView.getPanoramaByLocation(marker.position, radius, getStreetView);
                infowindow.open(map, marker);

            

        }

        function showlistings() {
            var bounds = new google.maps.LatLngBounds();

            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
                bounds.extend(markers[i].position);
				 //map.fitBounds(bounds,20);
            }
		
          // new google.maps.fitBounds(bounds);
		
        }

        function hidelistings() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }

        }

        function makeMarkerIcon(markerColor) {
            var markerImage = new google.maps.MarkerImage(
                'http://chart.googleapis.com/chart?chst=d_map_spin&chld=1.15|0|' + markerColor + '|40|_|%E2%80%A2',
                new google.maps.Size(21, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(10, 34),
                new google.maps.Size(21, 34));
            return markerImage;
        }


        function toggleDrawing(drawingManager) {
            if (drawingManager.map) {
                drawingManager.setMap(null);
                if (polygon) {
                    polygon.setMap(null);
                }
            } else {
                drawingManager.setMap(map);
            }
        }

        function searchWithinPolygon() {

            var markerCount = 0;
            for (var i = 0; i < markers.length; i++) {
                if (google.maps.geometry.poly.containsLocation(markers[i].position, polygon)) {
                    markers[i].setMap(map);
                    markerCount++;

                } else {
                    markers[i].setMap(null);
                }
                document.getElementById('marker-count').innerHTML ="<p>Total:" + ' ' + markerCount  + ' ' + "Trades Found</p>";
            }
        }
    }
        // initMap();
   initMap();

    };
	
		  reader.readAsText(file);
	});
      
 
    //  reader.readAsText(file);
	};
