<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="Resources/css/map.css">
</head>
<body>
                
                     
					
					      <div id="map"></div>
						   <div class="row">
                      <div class="col s3">
					       Select a CSV file:
					    <input type="file" id="fileInput" >
						    <!--<input type="file" id="filename" name="filename"/>-->
                 <div id="options-box">
                     <input type="button" id="show-listings" class="waves-effect  waves-dark btn-small indigo accent-2" value = "SHOW LISTINGS">
                     <input type="button" id= "hide-listings" class="waves-effect  waves-dark btn-small blue" value = "HIDE LISTINGS">
                 </div>
				 <hr>
				 <input type="button" id="toggle-drawings" class="waves-effect waves-light btn-small red" value="Drawing Tools">
				 <div id="areaInfo"></div>
				 <hr>
				 <div id="marker-count"></div>			 			
				 
				 <span>with in</span>
				 <div class="input-field">
				 <select id="max-duration">
				 <option value='10'>10 minutes</option>
				 <option value='20'>20</option>
				 <option value='30'>30</option>
				 <option value='40'>40</option>
				 <option value='50'>50</option>
				 <option value='60'>60</option>
				 <option value='70'>70</option>
				 <option value='80'>80</option>
				 </select>
				 </div>
				
				  <div class="input-field">
				  <select id="mode">
				 <option value='DRIVING'>DRIVING</option>
				 <option value='WALKING'>WALKING</option>
				 <option value='BICYCLING'>BICYCLING</option>
				 <option value='TRANSIT'>TRANSIT</option>
				 
				 </select>
				  </div>
				 <span class="text">Of</span>
				 <input type="text" id="search-within-time-text" placeholder="example">
				 <input type="button" id="search-within-time" value="GO">
				 
				  </div>
   
        
		  <pre id="fileDisplayArea"></pre>
    </div>
				 </div>
			 
      
                      </div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?language=es&libraries=geometry,drawing,places&key=AIzaSyA71rwaTVAgSVVzXbse7nMStRtMFnycn5c"></script>
<script src="Resources/js/maps.js"></script>
<!--<script src="Resources/js/directions.js"></script>-->
<!--<script src="Resources/js/fileUploadMap.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>

 $(document).ready(function(){
    $('select').formSelect();
  })
</script>
		
</body>
</html>